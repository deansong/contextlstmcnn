from __future__ import division
import sys
import os
import glob
import pickle
#import nltk.data
import numpy as np
from keras.preprocessing.text import Tokenizer
from sklearn.preprocessing import label_binarize
from sklearn.cross_validation import train_test_split
sys.path.append("ulti")
from ulti import *
from  BuildModels import Build_NN_Model
from w2vFOFE import FOFEW2V, DataProcess, DataProcess2class
from genLabel import genSentLabel
import gc
from optparse import OptionParser
from keras.models import load_model
from keras import regularizers
import time
import random
from sklearn import svm, linear_model

def trainBatchSmall(xtrain,ytrain, c_weight, model, w2v, nb_epoch=50, batchSize=64, xtest=None, ytest=None, sampleEachClass=10000):
    blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = model
    blstmcnnFofe.summary()
    useall = True
    trainingData = w2v.w2vdoc2fofesentWindowSample(xtrain,ytrain, useAll = True)
    allClassData = [[] for _ in range(len(w2v.avilableLabels))]

    xtrainnew = [] 
    xpretrain = []
    xlattrain = []
    ytrainnew = []


    for singleData in trainingData:
        currentx, currentpre, currentlat, currentori, currenty, prey, laty, selected = singleData
        if selected == True:
            if len(w2v.avilableLabels) > 2:
                yid = currenty.index(1)
            else:
                yid = currenty[0]
            allClassData[yid].append(singleData)
    for eachClassData in allClassData:
        random.shuffle(eachClassData)
        for singleData in eachClassData[:sampleEachClass]:
            currentx, currentpre, currentlat, currentori, currenty, prey, laty, selected = singleData
            xtrainnew.append(currentx)
            xpretrain.append(currentpre)
            xlattrain.append(currentlat)
            ytrainnew.append(currenty)
    print(len(xtrainnew))
    allClassData = []
    xtrain = []
    ytrain = []

    trainLoss = blstmcnnFofe.fit([np.array(xtrainnew),np.array(xpretrain),np.array(xlattrain)], np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize)

    trainLoss = blstmcnn.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize)

    trainLoss = blstmOnly.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize)

    trainLoss = cnnOnly.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize)

    return model

def trainBatchFull(xtrain,ytrain, c_weight, model, w2v, nb_epoch=50, batchSize=64, xtest=None, ytest=None):
    blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = model
    blstmcnnFofe.summary()
    useall = True
    trainingData = w2v.w2vdoc2fofesentWindowSample(xtrain,ytrain, useAll = True)
    allClassData = [[] for _ in range(len(w2v.avilableLabels))]

    xtrainnew = [] 
    xpretrain = []
    xlattrain = []
    ytrainnew = []


    for singleData in trainingData:
        currentx, currentpre, currentlat, currentori, currenty, prey, laty, selected = singleData
        xtrainnew.append(currentx)
        xpretrain.append(currentpre)
        xlattrain.append(currentlat)
        ytrainnew.append(currenty)
    print(len(xtrainnew))
    allClassData = []
    xtrain = []
    ytrain = []

    trainLoss = blstmcnnFofe.fit([np.array(xtrainnew),np.array(xpretrain),np.array(xlattrain)], np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize, class_weight=c_weight)
    trainLoss = blstmcnn.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize, class_weight=c_weight)
    trainLoss = blstmOnly.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize, class_weight=c_weight)
    trainLoss = cnnOnly.fit(np.array(xtrainnew), np.array(ytrainnew), epochs=nb_epoch, batch_size=batchSize, class_weight=c_weight)

    return model



def trainBatchExp(xtrain,ytrain, c_weight, model, w2v, nb_epoch=50, batchSize=64, xtest=None, ytest=None, sample=False):
    blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = model
    blstmcnnFofe.summary()
    if sample == True:
        useall = False
    else:
        useall = True

    for epoch in range(nb_epoch):
        trainingData = w2v.w2vdoc2fofesentWindowSample(xtrain,ytrain, windowSize=5, useAll= useall)
        i=0
        xtrainnew = []
        xpretrain = []
        xlattrain = []
        ytrainnew = []
        allTrainLoss = [[] for _ in range(4)]
        allTrainAccu = [[] for _ in range(4)]
        classCount = [0]*len(w2v.avilableLabels)
        print('epoch: ', epoch)
        for currentx, currentpre, currentlat, currentori, currenty, prey, laty, selected in trainingData:
            if selected == True:
                #print(currenty)
                if len(w2v.avilableLabels) > 2:
                    yid = currenty.index(1)
                else:
                    yid = currenty[0]
                xtrainnew.append(currentx)
                xpretrain.append(currentpre)
                xlattrain.append(currentlat)
                ytrainnew.append(currenty)
                i+=1
                classCount[yid]+=1
                    
            if len(xtrainnew) == batchSize:
                if sample:
                    topClass = classCount.index(max(classCount))
                    c_weight={}
                    topLabel = w2v.avilableLabels[topClass]
                    if topClass == 'noLabel':
                        for wid in range(len(classCount)):
                            c_weight[count_id] = 1.0
                    else:
                        c_weight = {}
                        for wid in range(len(classCount)):
                            if classCount[wid] != 0:
                                c_weight[wid] = float(classCount[topClass]/classCount[wid])
                            else:
                                c_weight[wid] = 1.0
                #print(c_weight)
                trainLoss = blstmcnnFofe.train_on_batch(x=[np.array(xtrainnew),np.array(xpretrain),np.array(xlattrain)], y=np.array(ytrainnew), class_weight = c_weight)
                allTrainLoss[0].append(trainLoss[0])
                allTrainAccu[0].append(trainLoss[1])

                trainLoss = blstmcnn.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
                allTrainLoss[1].append(trainLoss[0])
                allTrainAccu[1].append(trainLoss[1])

                trainLoss = blstmOnly.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
                allTrainLoss[2].append(trainLoss[0])
                allTrainAccu[2].append(trainLoss[1])

                trainLoss = cnnOnly.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
                allTrainLoss[3].append(trainLoss[0])
                allTrainAccu[3].append(trainLoss[1])

                xtrainnew = []
                xpretrain = []
                xlattrain = []
                ytrainnew = []
                classCount = [0,0,0,0]
        if len(xtrainnew) > 0:
            trainLoss = blstmcnnFofe.train_on_batch(x=[np.array(xtrainnew),np.array(xpretrain),np.array(xlattrain)], y=np.array(ytrainnew), class_weight = c_weight)
            allTrainLoss[0].append(trainLoss[0])
            allTrainAccu[0].append(trainLoss[1])

            trainLoss = blstmcnn.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
            allTrainLoss[1].append(trainLoss[0])
            allTrainAccu[1].append(trainLoss[1])

            trainLoss = blstmOnly.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
            allTrainLoss[2].append(trainLoss[0])
            allTrainAccu[2].append(trainLoss[1])

            trainLoss = cnnOnly.train_on_batch(x=np.array(xtrainnew), y=np.array(ytrainnew), class_weight = c_weight)
            allTrainLoss[3].append(trainLoss[0])
            allTrainAccu[3].append(trainLoss[1])

        print('blstmcnnFofe',sum(allTrainLoss[0])/len(allTrainLoss[0]), sum(allTrainAccu[0])/len(allTrainAccu[0]))
        print('blstmcnn',sum(allTrainLoss[1])/len(allTrainLoss[1]), sum(allTrainAccu[1])/len(allTrainAccu[1]))
        print('blstm',sum(allTrainLoss[2])/len(allTrainLoss[2]), sum(allTrainAccu[2])/len(allTrainAccu[2]))
        print('cnn',sum(allTrainLoss[3])/len(allTrainLoss[3]), sum(allTrainAccu[3])/len(allTrainAccu[3]))
    return model

parser = OptionParser()
parser.add_option("-i", "--inputDir", dest="inputDir", help="input dir in following hierarchy dir>patient>documents/annotations")
parser.add_option("-p", "--pickledInput", dest="pickledInput", help="pre-processed input in python pickle format")
parser.add_option("-m", "--loadModel", dest="loadModel", help="load trianed model")
parser.add_option("-t", "--train", dest="train", help = "train model, give model saving location")
parser.add_option("--testIDs", dest="testIDs", help="load test IDs")
parser.add_option("--trainIDs", dest="trainIDs", help="load train IDs")
parser.add_option("--saveModel", dest="saveModel", help = "save trained model")
parser.add_option("--trainTestSplit", dest="trainTestSplit",action='store_true', help = "split file into training (80%) and testing parts(20%)")
parser.add_option("--epochs", dest="epochs", default=50, help = "number of epochs for training, default 50", type="int")
parser.add_option("--sentenceLevelEvaluation", dest="sentenceLevelEvaluation",action='store_true', help = "sentence level evaluation")
parser.add_option("--sentenceLevel2ClassEvaluation", dest="sentenceLevel2ClassEvaluation",action='store_true', help = "sentence level evaluation only 2 classes")
parser.add_option("--negSentenceLevel2ClassEvaluation", dest="negSentenceLevel2ClassEvaluation",action='store_true', help = "neg tool sentence level evaluation only 2 classes")
parser.add_option("--documentLevelEvaluation", dest="documentLevelEvaluation",action='store_true', help = "document level evaluation")
parser.add_option("--patientLevelEvaluation", dest="patientLevelEvaluation",action='store_true', help = "patient level evaluation")
parser.add_option("--ebdSize", dest="ebdSize", default=50, help = "embedding size")
parser.add_option("--nFoldsSplit", dest="nFoldsSplit", help = "prefix of n folds split")
parser.add_option("--earlystop", dest="earlystop", help = "early stop")
parser.add_option("--train2class", dest="train2class", action='store_true', help = "train 2 class model")
parser.add_option("--sample", dest="sample", action='store_true', help = "sample training")
parser.add_option("--smallTrain", dest="smallTrain", action='store_true', help = "only sample 10000 training instanses")
parser.add_option("--trainSvm", dest="trainSvm", help = "train svm")
parser.add_option("--trainMaxEnt", dest="trainMaxEnt", help = "train maximum entropy")
parser.add_option("--loadSvmModel", dest="loadSvmModel", help="load trianed svm model")
parser.add_option("--negTool", dest="negTool", action='store_true', help = "evaluate negation detection tool")

blstmcnnFofe = None
blstmcnn = None
blstmOnly = None
cnnOnly = None
svmModel = None
maxEntModel = None

returnOriSent = False

options, arguments = parser.parse_args()

if options.negTool:
    import negationDetection
    returnOriSent = True

if options.train2class:
    w2v = DataProcess2class()
else:
    w2v = DataProcess()
    
w2v.ebd_size = options.ebdSize
nn_model = Build_NN_Model(len(w2v.avilableLabels))


if options.inputDir:
    allLabeledList = genSentLabel(options.inputDir)
    print(allLabeledList[:2])

if options.pickledInput:
    with open(options.pickledInput,'rb') as fp:
        allLabeledList = pickle.load(fp)

if options.trainTestSplit:
    trainID, testID = train_test_split(range(len(allLabeledList)), test_size=0.2)
else:
    trainID = range(len(allLabeledList))
    testID = range(len(allLabeledList))

if options.nFoldsSplit:
    allDocIds = range(len(allLabeledList))
    from sklearn.model_selection import KFold
    kf = KFold(n_splits=5, shuffle=True)
    foldCount = 1
    for item in kf.split(allDocIds):
        currentTrainId = item[0].tolist()
        currentTestId = item[1].tolist()
        with open(options.nFoldsSplit+'trainIds'+str(foldCount)+'.pkl','wb') as fpkl:
            pickle.dump(currentTrainId, fpkl)
        with open(options.nFoldsSplit+'testIds'+str(foldCount)+'.pkl','wb') as fpkl:
            pickle.dump(currentTestId, fpkl)
        foldCount+=1


if options.trainIDs:
    with open(options.trainIDs,'rb') as fin:
        trainID = pickle.load(fin)

if options.testIDs:
    with open(options.testIDs,'rb') as fin:
        testID = pickle.load(fin)

if options.loadModel:
    blstmcnnFofe = load_model(options.loadModel+'blstmcnnFofe.model')
    blstmcnn = load_model(options.loadModel+'blstmcnn.model')
    blstmOnly = load_model(options.loadModel+'blstm.model')
    cnnOnly = load_model(options.loadModel+'cnn.model')
    w2v.load_w2v(options.loadModel+'w2v.model')

if options.loadSvmModel:
    with open(options.loadSvmModel+'svmbow.model', 'rb') as fp:
        svmModel = pickle.load(fp) 
    with open(options.loadSvmModel+'bow.model', 'rb') as fp:
        w2v = pickle.load(fp)


if options.trainSvm or options.trainMaxEnt:
    w2v.sentence = bow_training_sents(allLabeledList, trainID)
    w2v.buildDict()
    allTraining = svm_sents(allLabeledList, trainID)
    xtrain, ytrain, total = w2v.sent2bow(allLabeledList, trainID)
    allClassData = [[] for _ in range(len(w2v.avilableLabels))]
    sampleEachClass = 10000

    for singleDataId in range(len(xtrain)):
        currentx = xtrain[singleDataId]
        currenty = ytrain[singleDataId]
        allClassData[currenty].append([currentx, currenty])

    xtrain = []
    ytrain = []
    for eachClassData in allClassData:
        print(len(eachClassData))
        random.shuffle(eachClassData)
        for dataPair in eachClassData[:sampleEachClass]:
            currentx, currenty = dataPair
            xtrain.append(currentx)
            ytrain.append(currenty)
    print(len(xtrain))
    allClassData = []

    if options.trainSvm:
        svmModel = svm.SVC(decision_function_shape='ovr')
        svmModel.fit(xtrain, ytrain)
        with open(options.trainSvm+'svmbow.model', 'wb') as fp:
            pickle.dump(svmModel, fp)
        with open(options.trainSvm+'bow.model', 'wb') as fp:
            pickle.dump(w2v, fp)
    if options.trainMaxEnt:
        maxEntModel = linear_model.LogisticRegression(C=1e5)
        maxEntModel.fit(xtrain, ytrain)
        with open(options.trainMaxEnt+'maxEnt.model', 'wb') as fp:
            pickle.dump(maxEntModel, fp)
        with open(options.trainMaxEnt+'bow.model', 'wb') as fp:
            pickle.dump(w2v, fp)
        


if options.train:
    w2v.sentence = w2v.w2v_training_sents(allLabeledList, trainID)
    w2v.build_w2v()
    print(w2v.w2vModel.most_similar(positive=['suicide']))
    xtrain, ytrain, orix, total = w2v.doc2W2Vdoc(allLabeledList, trainID)
    topClass = total.index(max(total))
    c_weight = {}
    for count_id in range(len(total)):
        if total[count_id] != 0:
            c_weight[count_id] = float(total[topClass]/total[count_id])
        else:
            c_weight[count_id] = 1.0
    print(c_weight)
    
    blstmcnnFofe = nn_model.contextBlstmcnn()
    blstmcnn = nn_model.blstmcnn()
    blstmOnly = nn_model.blstm_only()
    cnnOnly = nn_model.cnn_only()
    if options.smallTrain:
        blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = trainBatchSmall(xtrain,ytrain, c_weight, [blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly], w2v, nb_epoch=options.epochs, batchSize=64)
    elif options.sample:
        blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = trainBatchExp(xtrain,ytrain, c_weight, [blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly], w2v, nb_epoch=options.epochs, batchSize=64, sample=options.sample)
    else:
        blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly = trainBatchFull(xtrain,ytrain, c_weight, [blstmcnnFofe, blstmcnn, blstmOnly, cnnOnly], w2v, nb_epoch=options.epochs, batchSize=64)

    w2v.save_w2v(options.train+'w2v.model')
    blstmcnnFofe.save(options.train+'blstmcnnFofe.model')
    blstmcnn.save(options.train+'blstmcnn.model')
    blstmOnly.save(options.train+'blstm.model')
    cnnOnly.save(options.train+'cnn.model')


if options.sentenceLevelEvaluation or options.documentLevelEvaluation or options.patientLevelEvaluation:
    docId = []
    blstmcnnFofeEvaluation = []
    blstmcnnEvaluation = []
    blstmOnlyEvaluation = []
    cnnOnlyEvaluation = []
    svmModelEvaluation = []
    maxEntModelEvaluation = []
    negToolEvaluation = []
    sentY = []    
    print('sent level testing')
    for currentID in testID:
        if blstmcnnFofe or blstmcnn or blstmOnly or cnnOnly or options.negTool:
            xtest, ytest, xtestori, total_test = w2v.doc2W2Vdoc(allLabeledList, [currentID],oriSent=returnOriSent)
            testingData = w2v.w2vdoc2fofesentWindowSample(xtest,ytest, oriX=xtestori, useAll=True)
            xtestnew = []
            xpretest = []
            xlattest = []
            ytestnew = []
            negToolPredTmpList = []
            for currentx, currentpre, currentlat, currentori, currenty, prey, laty, selected in testingData:
                if selected:
                    xtestnew.append(currentx)
                    xpretest.append(currentpre)
                    xlattest.append(currentlat)
                    ytestnew.append(currenty)
                    if options.negTool:
                        print(currentori)
                        ndpredi = negationDetection.assessDocument(currentori)['prediction']
                        print(currenty)
                        print(ndpredi)
                        if ndpredi == True:
                            ndtranspredi = [0]
                        else:
                            ndtranspredi = [1]
                        negToolPredTmpList.append(ndtranspredi)
            if len(ytestnew) > 0:
                xtestnew = np.array(xtestnew)
                xpretest = np.array(xpretest)
                xlattest = np.array(xlattest)
                ytestnew = np.array(ytestnew)
                sentY.append(ytestnew)
                docId.append(currentID)
            #print(len(xtestnew))
            #print(ytestnew[0])
                if blstmcnnFofe:
                    blstmcnnfofeprediction = blstmcnnFofe.predict([xtestnew,xpretest,xlattest])
                    blstmcnnFofeEvaluation.append(blstmcnnfofeprediction)
                    
                if blstmcnn:
                    blstmcnnprediction = blstmcnn.predict(xtestnew)
                    blstmcnnEvaluation.append(blstmcnnprediction)

                if blstmOnly:
                    blstmOnlyprediction = blstmOnly.predict(xtestnew)
                    blstmOnlyEvaluation.append(blstmOnlyprediction)

                if cnnOnly:
                    cnnOnlyprediction = cnnOnly.predict(xtestnew)
                    cnnOnlyEvaluation.append(cnnOnlyprediction)

                if options.negTool:
                    negToolEvaluation.append(np.array(negToolPredTmpList))

        elif svmModel or maxEntModel:
            print('svmevaluation')
            xtest, ytest,total_test = w2v.sent2bow(allLabeledList, [currentID])
            if len(ytest) > 0:
                sentY.append(np.array(ytest))
                docId.append(currentID)
                if svmModel:
                    svmprediction = svmModel.predict(xtest)
                    svmModelEvaluation.append(svmprediction)

                if maxEntModel:
                    maxEntprediction = maxEntModel.predict(xtest)
                    maxEntModelEvaluation.append(maxEntprediction)

    ally = np.concatenate(sentY)

    if options.negTool:
        print('negTool Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(negToolEvaluation) ,ally)

    if blstmcnnFofe:
        print('contextBlstmCnn Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(blstmcnnFofeEvaluation) ,ally)

    if blstmcnn:
        print('blstmCnn Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(blstmcnnEvaluation), ally)

    if blstmOnly:
        print('blstm Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(blstmOnlyEvaluation) ,ally)

    if cnnOnly:
        print('cnn Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(cnnOnlyEvaluation), ally)

    if svmModel:
        print('svm Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(svmModelEvaluation), ally, onehot=False)

    if maxEntModel:
        print('maxEnt Sentence Level Evaluation')
        evaluation(w2v.avilableLabels, np.concatenate(maxEntModelEvaluation) ,ally, onehot=False)




if options.documentLevelEvaluation or options.patientLevelEvaluation:
    # 0 suicide, 1 non-suicide
    blstmcnnFofeEvaluationDoc = []
    blstmcnnEvaluationDoc = []
    blstmOnlyEvaluationDoc = []
    cnnOnlyEvaluationDoc = []
    svmModelEvaluationDoc = []
    maxEntModelEvaluationDoc = []
    negToolEvaluationDoc = []
    startTime = time.time()

    if options.negTool:
        print('negation detection Document Level Evaluation')
        negToolEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, negToolEvaluation, sentY, docId)    

    if blstmcnnFofe:
        print('contextBlstmCnn Document Level Evaluation')
        blstmcnnFofeEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, blstmcnnFofeEvaluation , sentY, docId)

    if blstmcnn:
        print('blstmCnn Document Level Evaluation')
        blstmcnnEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, blstmcnnEvaluation, sentY, docId)

    if blstmOnly:
        print('blstm Document Level Evaluation')
        blstmOnlyEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, blstmOnlyEvaluation, sentY, docId)

    if cnnOnly:
        print('cnn Document Level Evaluation')
        cnnOnlyEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, cnnOnlyEvaluation, sentY, docId)

    if svmModel:
        print('svm Document Level Evaluation')
        svmModelEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, svmModelEvaluation, sentY, docId, onehot=False)

    if maxEntModel:
        print('maxEnt Document Level Evaluation')
        maxEntModelEvaluationDoc = evaluationDocLevel(w2v.avilableLabels, maxEntModelEvaluation ,sentY, docId, onehot=False)
     
if options.patientLevelEvaluation:
    patientDict = genPatientList(allLabeledList,docId)
    if options.negTool:
        print('negation detection Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, negToolEvaluationDoc)    

    if blstmcnnFofe:
        print('contextBlstmCnn Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, blstmcnnFofeEvaluationDoc)

    if blstmcnn:
        print('blstmCnn Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, blstmcnnEvaluationDoc)

    if blstmOnly:
        print('blstm Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, blstmOnlyEvaluationDoc)

    if cnnOnly:
        print('cnn Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, cnnOnlyEvaluationDoc)

    if svmModel:
        print('svm Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, svmModelEvaluationDoc)

    if maxEntModel:
        print('maxEnt Patient Level Evaluation')
        evaluationPatLevel(w2v.avilableLabels, patientDict, maxEntModelEvaluationDoc)
     
